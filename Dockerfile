FROM python:3-alpine

WORKDIR /app
COPY requirements.txt /app/requirements.txt
RUN mkdir db && pip3 install -r requirements.txt --no-cache-dir 
COPY . /app


EXPOSE 8000
VOLUME ["/app/db"]
#ENTRYPOINT ["python3"]
#CMD ["manage.py", "runserver","0.0.0.0:8000"]

CMD ./init.sh && python3 manage.py runserver 0.0.0.0:8000
